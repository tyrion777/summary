## MySQL的安装与使用

### 安装前说明

1. Linux系统及工具的准备

- 虚拟机：CentOS7
- 远程连接工具：Xshell
- CentOS6和CentOS7区别
```
1. 防火墙：6是iptables，7是firewalld  
2. 启动服务的命令：6是service，7是systemctl
```

2. 查看是否安装过MySQL

- 如果你是用rpm安装, 检查一下RPM PACKAGE：

```bash
rpm -qa | grep -i mysql # -i 忽略大小写
```

- 检查mysql service：

```bash
systemctl status mysqld.service
```

3. MySQL的卸载

```bash
# 1. 关闭 mysql 服务
systemctl stop mysqld.service

# 2. 查看当前 mysql 安装状况
rpm -qa | grep -i mysql # yum list installed | grep mysql

# 3. 卸载上述命令查询出的已安装程序
yum remove mysql-community-client.x86_64 mysql-community-server.x86_64

# 4. 删除 mysql 相关文件
find / -name mysql
rm -rf xxx

# 5.删除 my.cnf
rm -rf /etc/my.cnf
```
### Linux安装

1. MySQL的4大版本

- MySQL Community Server 社区版本，开源免费，自由下载，但不提供官方技术支持，适用于大多数普通用户

- MySQL Enterprise Edition 企业版本，需付费，不能在线下载，可以试用30天。提供了更多的功能和更完备的技术支持，更适合于对数据库的功能和可靠性要求较高的企业客户

- MySQL Cluster 集群版，开源免费。用于架设集群服务器，可将几个MySQL Server封装成一个Server。需要在社区版或企业版的基础上使用

- MySQL Cluster CGE 高级集群版，需付费

2. 下载MySQL指定版本

官网：https://www.mysql.com


安装方式 | 特点
----------|----------
rpm  |安装简单，灵活性差，无法灵活选择版本、升级
rpm repository  |安装包极小，版本安装简单灵活，升级方便，需要联网安装
通用二进制包  |安装比较复杂，灵活性高，平台通用性好
源码包  |安装最复杂，时间长，参数设置灵活，性能好

3. CentOS7下检查MySQL依赖

```bash
# 1. 检查/tmp临时目录权限
chmod -R 777 /tmp

# 2. 安装前，检查依赖
rpm -qa|grep libaio
rpm -qa|grep net-tools
```

4. CentOS7下MySQL安装过程

```bash
# 在安装目录 /opt 执行
rpm -ivh mysql-community-common-8.0.25-1.el7.x86_64.rpm  

rpm -ivh mysql-community-client-plugins-8.0.25-1.el7.x86_64.rpm 

rpm -ivh mysql-community-libs-8.0.25-1.el7.x86_64.rpm 

rpm -ivh mysql-community-client-8.0.25-1.el7.x86_64.rpm 

rpm -ivh mysql-community-server-8.0.25-1.el7.x86_64.rpm

# rpm 是Redhat Package Manage缩写，通过RPM的管理，用户可以把源代码包装成以rpm为扩展名的文件形式，易于安装

# -i , --install 安装软件包

# -v , --verbose 提供更多的详细信息输出

# -h , --hash 软件包安装的时候列出哈希标记 (和 -v 一起使用效果更好)，展示进度条

# 安装过程出现问题解决 yum remove mysql-libs

# 查看MySQL版本
mysql --version

# 服务的初始化
mysqld --initialize --user=mysql

# 查看密码 A temporary password is generated for root@localhost: ljrF/6u,:fC+
cat /var/log/mysqld.log

# 启动
systemctl start mysqld.service

# 关闭
systemctl stop mysqld.service

# 重启
systemctl restart mysqld.service

# 查看状态
systemctl status mysqld.service

# 查看进程
ps -ef | grep -i mysql

# 查看MySQL服务是否自启动
systemctl list-unit-files | grep mysqld.service

# 设置自启动 enable | disable
systemctl enable mysqld.service
```

### 登录

1. 首次登录

```bash
# ljrF/6u,:fC+
mysql -hlocalhost -P3306 -uroot -p
```

2. 修改密码

```bash
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root123';
```

3. 设置远程登录

`关闭防火墙`
```bash
systemctl start firewalld.service

systemctl stop firewalld.service

systemctl status firewalld.service

# 设置开机启用防火墙 
systemctl enable firewalld.service 

# 设置开机禁用防火墙 
systemctl disable firewalld.service
```

`开启端口`
```bash
# 查看开放的端口号
firewall-cmd --list-all

# 设置开放的端口号
firewall-cmd --add-service=http --permanent
firewall-cmd --add-port=3306/tcp --permanent

# 重启防火墙
firewall-cmd --reload
```

`Linux下修改配置`
```sql
use mysql;

select Host,User from user;

-- 修改Host为通配符%  在生产环境下不能为了省事将host设置为%，这样做会存在安全问题
update user set host = '%' where user ='root';

-- 刷新权限
flush privileges;
```

### 密码强度

1. 不同版本设置密码

2. MySQL8之前的安全策略

3. MySQL8的安全策略

4. 卸载插件、组件

### 字符集

1. MySQL字符集

```sql
-- 查看默认使用的字符集 或者 show variables like '%char%';
show variables like 'character%';


```

2. 各级别的字符集

3. 字符集与比较规则

`utf8 与 utf8mb4`

- utf8mb3 ：阉割过的 utf8 字符集，只使用1～3个字节表示字符

- utf8mb4 ：正宗的 utf8 字符集，使用1～4个字节表示字符

`比较规则`

后缀 | 英文释义 | 描述
----------|----------|----------
_ai |accent insensitive|不区分重音
_as | accent sensitive |区分重音
_ci |case insensitive |不区分大小写
_cs |case sensitive |区分大小写
_bin| binary |以二进制方式比较

4. 请求到响应过程中字符集的变化

### SQL大小写

1. Windows和Linux平台区别

2. Linux下大小写规则设置

3. SQL编写建议
```
1、 关键字和函数名称全部大写

2、 数据库名、表名、表别名、字段名、字段别名等全部小写

3、 SQL 语句必须以分号结尾
```

### sql_mode的合理设置

1. 宽松模式 vs 严格模式

2. 宽松模式再举例

3. 模式查看和设置