## MySQL的事务日志

### 事务的ACID是有哪些机制实现

1. 原子性 由`回滚日志`实现
2. 一致性 由`回滚日志`实现
3. 隔离性 由`锁机制`实现
4. 持久性 由`重做日志`实现

### redo日志

`为什么需要redo日志`

<img :src="$withBase('/为什么需要redo日志.png')" alt="为什么需要redo日志">

`优点`

- 降低了刷盘频率
- 占用的空间非常小

`特点`

- redo日志是顺序写入磁盘的
- 事务执行过程中，redo log不断记录

`组成`

- 重做日志的缓冲 (redo log buffer) ，保存在内存中，是易失的
```sql
-- 重做日志缓存的大小 默认16M 16777216B
show variables like '%innodb_log_buffer_size%';
```
- 重做日志文件 (redo log file) ，保存在硬盘中，是持久的

`流程`

<img :src="$withBase('/redo日志流程.png')" alt="redo日志流程">

1. 先将原始数据从磁盘中读入内存中来，修改数据的内存拷贝
2. 生成一条重做日志并写入redo log buffer，记录的是数据被修改后的值
3. 当事务commit时，将redo log buffer中的内容刷新到 redo log file，对 redo log file采用追加 写的方式
4. 定期将内存中修改的数据刷新到磁盘中

`刷盘策略`

<img :src="$withBase('/redo日志刷盘策略.png')" alt="redo日志刷盘策略">

> 注意

redo log buffer刷盘到redo log file的过程并不是真正的刷到磁盘中去，只是刷入到 `文件系统缓存 `（page cache）中去（这是现代操作系统为了提高文件写入效率做的一个优化），真正的写入会交给系统自己来决定  

```sql
-- redo日志的刷盘策略 0 1 2 
show variables like '%innodb_flush_log_at_trx_commit%';

-- 0 表示每次事务提交时不进行刷盘操作（系统默认master thread每隔1s进行一次重做日志的同步）
-- 1 表示每次事务提交时都将进行同步，刷盘操作（ 默认值 ）
-- 2 表示每次事务提交时都只把 redo log buffer 内容写入 page cache，不进行同步。由os自己决定什么时候同步到磁盘文件
```






### undo日志