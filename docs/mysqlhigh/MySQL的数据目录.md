## MySQL的数据目录

### MySQL8.0的主要目录结构

```sql
-- 查看目录
find / -name mysql
```

1. 数据库文件的存放路径 

/var/lib/mysql/

```sql
show variables like 'datadir';
```

2. 相关命令目录 

/usr/bin（mysqladmin、mysqlbinlog、mysqldump等命令）和 /usr/sbin

3. 配置文件目录

/usr/share/mysql-8.0（命令及配置文件），/etc/mysql（如my.cnf）

### 数据库和文件系统的关系

1. 查看默认数据库

```sql
show databases;
```

> mysql

MySQL 系统自带的核心数据库，它存储了MySQL的用户账户和权限信息，一些存储过程、事件的定义信息，一些运行过程中产生的日志信息，一些帮助信息以及时区信息等

> information_schema

MySQL 系统自带的数据库，这个数据库保存着MySQL服务器 维护的所有其他数据库的信息 ，比如有哪些表、哪些视图、哪些触发器、哪些列、哪些索引。这些信息并不是真实的用户数据，而是一些描述性信息，有时候也称之为 元数据

> performance_schema

MySQL 系统自带的数据库，这个数据库里主要保存MySQL服务器运行过程中的一些状态信息，可以用来 监控 MySQL 服务的各类性能指标 。包括统计最近执行了哪些语句，在执行过程的每个阶段都花费了多长时间，内存的使用情况等信息

> sys

MySQL 系统自带的数据库，这个数据库主要是通过 视图 的形式把 information_schema 和 performance_schema 结合起来，帮助系统管理员和开发人员监控 MySQL 的技术性能

2. 数据库在文件系统中的表示

3. 表在文件系统中的表示