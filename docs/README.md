---
home: true
heroText: 好记性不如烂笔头
tagline: 欢迎来到我的空间
actionLink: /zh/guide/
features:
- title: 系统
  details: 形成知识树
- title: 全面
  details: 大处着眼，小处着手
- title: 开放
  details: 虚心学习，共同进步
footer: 更快 更高 更强
---
