## Docker简介

### Docker理念

一次镜像，到处运行

### 传统虚拟机和容器的区别

`区别`

<img :src="$withBase('/Docker和虚拟机对比.png')" alt="Docker和虚拟机对比">

`场景`

<img :src="$withBase('/Docker应用场景.png')" alt="Docker应用场景">

### 下载地址

- 官网 http://www.docker.com
- 仓库 https://hub.docker.com/











