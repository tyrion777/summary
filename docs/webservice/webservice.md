## WebService

### 概念

Web服务是一种服务导向架构的技术，通过标准的Web协议提供服务，目的是保证不同平台的应用服务可以互操作



### 运行与访问过程

<img :src="$withBase('/ws-运行与访问.png')" alt="ws-运行与访问">

### 核心组件

1. XML和HTTP
2. SOAP：简单对象访问协议
3. WSDL：WebService描述语言
4. UDDI：统一描述、发现和集成协议



### AXIS

（Apache eXtensible Interaction System）阿帕奇可扩展交互系统，本质就是一个SOAP引擎，提供创建服务器端，客户端和网关SOAP操作的基本框架

官网

http://axis.apache.org/



### CXF

CXF = Celtix + XFire

官网

http://cxf.apache.org/



### Soap协议+ TCP/IP Moniter监控

SOAP:( Simple Object Access Protocol )是一种轻量的、简单的，基于XMLX的协议

可使应用程序在HTTP之上进行信息交换



com.test.cxf包名反写，这样提供外部接口的时候，网址刚好是cxf.test.com



一次WebService的调用，不是方法的调用，而是soap信息（xml格式规范的文档片段）之间的输入输出



总结

1. 客户端到UDDI上寻找Service目录
2. 客户端获得WSDL文件
3. 客户端按照WSDL文件的约束和规范创建SOAP客户端
4. 客户端通过SOAP访问Service



### WSDL文件解析

definitions：命名空间 根元素

<img :src="$withBase('/ws-def.png')" alt="ws-def">

types：webservice使用的数据

<img :src="$withBase('/ws-type.png')" alt="ws-type">

message：webservice使用的消息

请求参数和返回参数



portType：webservice执行的操作

一个是接口，一个是接口中的方法



binding：webservice使用的通信协议

特定端口类型的具体协议和数据格式规范的绑定



service：webservice对外暴露



### JAXB

JAXB = Java API For XML Bingding

Java对象与XML进行转换



### JAX-WS

JAX-WS = Java API For XML Web Service



是一组XML Web Service的Java API ,运行时实现会将这些调用转换成对应的SOAP消息



#### spring整合



### JAX-RS

REST风格的CXF









### 命令

```java
// 从网站wsdl编译出Java代码 

// 生成java类
wsdl2java http://218.242.137.194:55524/sys/webservice/kmSpAllocateAuditWebserviceService?wsdl

/*
可选参数
-client 生成客户端调用代码

*/

// 生成jar包

```



