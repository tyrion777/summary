module.exports = {
  title: 'CMX的学习笔记',
  description: '',
  // 设置站点根路径
  base: '/summary/',

  themeConfig: {

    // 顶部导航栏
    nav: require("./nav.js"),

    // 侧边导航栏
    sidebar: require("./sidebar.js"),

    sidebarDepth: 2,

    lastUpdated: 'Last Updated', // string | boolean

    // 页面滚动
    smoothScroll: true,


  }
}