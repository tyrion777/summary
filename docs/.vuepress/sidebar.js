module.exports = {

    // mysql高级
    '/mysqlhigh/': [
        'MySQL的安装与使用',
        'MySQL的数据目录',
        '用户与权限管理',
        '逻辑架构',
        '存储引擎',
        '索引的数据结构',
        'InnoDB数据存储结构',
        '索引的创建与设计原则',
        '性能分析工具的使用',
        '索引优化与查询优化',
        '数据库的设计规范',
        '数据库其他调优策略',
        '事务基础知识',
        'MySQL的事务日志',
        '锁',
        '多版本并发控制',
        '其他数据库日志',
        '主从复制',
        '数据库备份与恢复',
    ],

    // mysql基础
    '/mysqlbase/': [
        '数据库概述与安装',
        'SELECT使用',
        '单行函数',
        '聚合函数',
        'DDL等的使用',
        '存储过程与函数',
        '其他数据库',
        '8.0新特性'
    ],

    // java
    '/java/': [
        '注释',
        '基本数据类型',
        '类变量和类方法',
        'ArrayList',
        'HashMap',
    ],

    // groovy
    '/groovy/': [
        '与Java的区别',
    ],

    // vue
    '/vue/': [
    ],

     // JavaScript
     '/javascript/': [
         'npm',
         '模块化',
         'webpack'
    ],

    // webservice
    '/webservice/': [
        'webservice',
    ],

    // spring
    '/spring/': [

    ],

    // springmvc
    '/springmvc/': [

    ],

    // springboot
    '/springboot/': [

    ],

    // springcloud
    '/springcloud/': [

    ],

    // 操作系统
    '/system/': [
        '硬件结构',
    ],

    // mybatis
    '/mybatis/': [
        'MyBatis简介',
        '搭建MyBatis',
        '核心配置文件详解',
        'MyBatis的增删改查',
        'MyBatis获取参数值的两种方式',
        'MyBatis的各种查询功能',
        '特殊SQL的执行',
        '自定义映射resultMap',
        '动态SQL',
        'MyBatis的缓存',
        'MyBatis的逆向工程',
        '分页插件'
    ],

    // hibernate
    '/hibernate/': [
        
    ],


    // 面试题
    '/problem/': [
        'mysql',
        'mq',
        'java',
        'spring',
        'springmvc',
        'springboot',
        'springcloud',
        'mybatis',
    ],

    // 烹饪
    '/cook/': [
        '炒',
        '蒸',
        '炸',
        '煮',
        '刀',
    ],

    // 数据结构
    '/data/': [
        '单向链表',
        '双向链表',
        '队列',
        '环形队列',
        '单向环形链表',
        '栈',
    ],

    // Dubbo
    '/dubbo/': [
    ],

    // Maven
    '/maven/': [
        'Pom的四层体系',
        '属性的声明和应用',
    ],

    // Linux
    '/linux/': [
        '入门',
        '安装',
        '登录',
        '用户管理',
        '实用指令',

    ],

    // nginx
    '/nginx/': [
       '配置文件',
       '反向代理',

    ],

    // DockerBase
    '/dockerbase/': [
        'Docker简介',
        'Docker安装',
        'Docker常用命令',
        'Docker镜像',
        '本地镜像发布到阿里云',
        '本地镜像发布到私有库',
        'Docker容器数据卷',
        'Docker常规安装简介',
    ],

    // DockerHigh
    '/dockerhigh/': [
        'Docker复杂安装',
        'DockerFile解析',
        'Docker微服务实战',
        'Docker网络',
        'DockerCompose容器编排',
        'Docker可视化工具',
        'Docker容器监控',
        '总结',
    ],

    // Redis
    '/redis/': [
        'NoSQL数据库简介',
        'Redis简介与安装',
        '常用五大数据类型',
        '配置文件',
        '发布和订阅',
        '新数据类型',
        'Jedis',
        'Redis与SpringBoot',
        '事务与锁机制',
        '持久化RDB',
        '持久化AOF',
        '主从复制',
        '集群',
        '应用问题解决',
        '6.0新功能',
    ],



    // fallback
    '/': [
        '',
    ]
}
