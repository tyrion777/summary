module.exports =
    [
        { text: '首页', link: '/' },

        {
            text: 'MySQL',
            items: [
                { text: '基础', link: '/mysqlbase/' },
                { text: '高级', link: '/mysqlhigh/' },
            ],
        },

        {
            text: 'ORM',
            items: [
                { text: 'Mybatis', link: '/mybatis/' },
                { text: 'Hibernate', link: '/hibernate/' },
            ],
        },

        {
            text: '版本管理',
            items: [
                { text: 'Git', link: '/git/' },
                { text: 'SVN', link: '/svn/' },
            ],
        },

        {
            text: '构建工具',
            items: [
                { text: 'Maven', link: '/maven/' },
                { text: 'Gradle', link: '/gradle/' },
            ],
        },

        {
            text: '核心',
            items: [
                { text: 'Java', link: '/java/' },
                { text: '设计模式', link: '/design/' },
                { text: '数据结构', link: '/data/' },
                { text: '网络', link: '/net/' },
                { text: 'OS', link: '/system/' },
            ],
        },

        {
            text: 'Spring全家桶',
            items: [
                { text: 'Spring', link: '/spring/' },
                { text: 'SpringMVC', link: '/springmvc/' },
                { text: 'SpringBoot', link: '/springboot/' },
                { text: 'SpringCloud', link: '/springcloud/' },
            ],
        },

        {
            text: 'MQ',
            items: [
                { text: 'RocketMq', link: '/rocketmq/' },
                { text: 'RabbitMq', link: '/rabbitmq/' },
            ],
        },

        {
            text: 'Docker',
            items: [
                { text: '基础', link: '/dockerbase/' },
                { text: '高级', link: '/dockerhigh/' },
            ],
        },

        {
            text: 'Front',
            items: [
                { text: 'JavaScript', link: '/javascript/' },
                { text: 'Vue', link: '/vue/' },
            ],
        },

        {
            text: '扩展',
            items: [
                { text: 'Groovy', link: '/groovy/' },
                { text: 'WebService', link: '/webservice/' },
                { text: 'Redis', link: '/redis/' },
                { text: 'Dubbo', link: '/dubbo/' },
                { text: 'Linux', link: '/linux/' },
                { text: 'Nginx', link: '/nginx/' },
                { text: '面试题', link: '/problem/' },
                { text: '烹饪', link: '/cook/' },
            ],
        },
    ]



