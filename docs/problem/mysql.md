## MySQL

### 1. MySQ索引的原理和数据结构能介绍一下吗？

```sql
/*
索引的原理：
    和书籍的目录页相似，通过查询目录来确定一个范围，最后找到该条数据的位置。
    根据聚簇索引查询数据
        需要先从根节点通过二分法，确定范围，然后一层一层往下找
    根据非聚簇（二级）索引查询数据
        也是先从根节点通过二分法，确定范围，然后一层一层往下找，不同于聚簇索引，
        二级索引的记录里面会保存着主键值，通过二级索引确定位置之后（如果要的是整条记录的话），
        还需要进行回表的操作，还是从根节点出发，找到叶子节点上的记录。如果要的数据就是建立二级
        索引的记录的话，不需要进行回表操作（覆盖索引）。
    其实就是牺牲了插入（更新，删除）数据时候需要构建索引的时间，来把随机io变成顺序io（创建数据时，
    会根据索引从小到大的顺序排列），来增加查询的效率。

索引的数据结构：
    采用B+树的结构，叶子节点存放记录，非叶子节点存放目录，换句话说，叶子节点存放的是数据页，
    非叶子节点存放的是目录页，叶子节点层的数据页通过双向链表连接，非叶子节点层的目录页也通过
    双向链表连接（根节点除外），页中的的记录则是通过单向链表连接
*/
```

### 2. b+树和b-树有什么区别？

```sql
/*
B+树
    只有叶子节点上才存储记录（聚簇索引），非叶子节点存储的是目录页，而目录页是仅仅存储主键id，
    占用的空间小，存放的记录自然比较多。对比B树，相同的空间，可以存放更多的目录项，使得整体的
    树高比较低，io次数更少，查询效率更高。
B树
    数据页和目录页不区分，既是数据页也是目录页，相较于B+树，相同的空间，存放的记录更少，会造成
    树的层级比较高，io次数更多，查询效率更低。
*/
```

### 3. MySQL聚簇索引和非聚簇索引的区别是什么？

```sql

```

### 4. 聚簇索引和非聚簇索引分别是怎么存储的？

```sql

```

### 5. 使用MySQL索引都有哪些原则？

```sql

```

### 6. MySQL复合（联合）索引如何使用？

```sql

```

### 7. MySQL事务隔离级别

```sql

```

### 8. 可串行化隔离级别的实现原理，如何避免幻读的

```sql

```

### 9. 为什么主键用自增不用UUID

```sql

```

### 10. 数据库事务ACID特性是怎么实现的

```sql

```

### 11. sql编写题--找到各科成绩都大于80的学生名称

```sql
CREATE TABLE `tb_test_course` (
  `id` int(11) NOT NULL COMMENT '主键自增id',
  `student_name` varchar(8) DEFAULT NULL COMMENT '学生名称',
  `course_name` varchar(8) DEFAULT NULL COMMENT '课程名称',
  `score` int(11) DEFAULT NULL COMMENT '得分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='面试-course-score表'

SELECT * FROM tb_test_course;

INSERT INTO tb_test_course VALUES (1,'张三','语文',79),(2,'张三','数学',80),
(3,'李四','数学',90),(4,'李四','英语',96),(5,'王五','数学',8),(6,'王五','英语',90);

-- 找到各科成绩都大于80的学生名称
-- 方式一 有点儿问题
SELECT
	student_name,
	score,
	course_name 
FROM
	tb_test_course 
GROUP BY
	student_name,
	course_name 
HAVING
	score > 80;

-- 方式二 正确解法 效率高
SELECT DISTINCT
	student_name 
FROM
	tb_test_course 
WHERE
	score > 80 
GROUP BY
	student_name,
	course_name
```

### 12. 设计一个B/S架构中的权限系统表

```sql

```




