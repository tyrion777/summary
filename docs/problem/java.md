## Java  

1. sleep()和wait()方法的区别

```

```

2. String--输出结果是什么
   
```java
package com.rong.metacmx.redis.entity;

public class Test {

    public static void main(String[] args) {
        StrStr strStr = new StrStr();
        strStr.str = "i am a str";
        NumberStr numberStr = new NumberStr();
        // 这里只是调用了方法，并没有实际赋值的操作
        numberStr.changeStr(strStr.str);
        System.out.println(strStr.str);
    }

}

class StrStr {
    public String str;
}

class NumberStr{
    void changeStr(String str){
        str = "i am a number";
    }
}

// 结果就是 i am a str
```

3. 基本类型--输出结果是什么

```java
package com.rong.metacmx.redis.entity;

public class Test {

    public static void main(String[] args) {
        double db = 2.02d;
        NumberStr numberStr = new NumberStr();
        numberStr.changeDouble(db);
        System.out.println(db);
    }
}

class NumberStr {

    void changeDouble(double db) {
        db = db - 1.0d;
    }
}

// 输出结果是 2.02
```

4. static 和 final 的含义和作用

```java
/*
1. static 修饰变量 --> 类变量 --> 类的所有实例对象共享 
    --> 放到堆内存(jdk1.8) --> 在类加载的时候就已经生成




/*

```

5. 重载(OverLoad)和重写(Override)的区别 (入参和返回值)

```java
/*
重载
    在同一个类中，多个同名方法可以存在，但是要求形参列表不一致

注意事项
    1. 方法名相同
    2. 必须不同(参数类型或者个数或者顺序，至少一样不同，参数名不要求)
    3. 返回类型(不要求)

重写
    方法名一样，返回值一样，入参一样
*/
```

6. equals() 和 hashCode() 作用

```

```





