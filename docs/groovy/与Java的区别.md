## 与Java的区别

### 默认导入

```groovy
// 以下的包都是默认导入的，不需要使用 Import
java.io.*

java.lang.*

java.math.BigDecimal

java.math.BigInteger

java.net.*

java.util.*

groovy.lang.*

groovy.util.*
```

### 多方法

在 Groovy 中，将被调用的方法是在运行时选择的。这称为运行时调度或多方法。
这意味着将在运行时根据参数的类型选择方法。在 Java 中，情况正好相反：方法是在编译时根据声明的类型选择的。

```groovy
int method(String arg) {
    return 1;
}
int method(Object arg) {
    return 2;
}
Object o = "Object";
int result = method(o);

// 在Java中 会根据Object类型去调用
method(o) == 2

// 在Groovy中 会根据 “Object” 字符串调用
method(o) == 1
```

### 包范围可见性

```groovy
class Person {
    // 表示字段私有
    @PackageScope String name
}
```

### GStrings

由于双引号字符串文字被解释为值，如果使用 Groovy 和 Java 编译器编译具有包含 `$` 字符的文字GString的类，
Groovy 可能会因编译错误而失败或产生细微不同的代码。

虽然通常情况下，Groovy 会在 API 声明参数类型之间进行自动GString转换String，但请注意接受Object参数然后检查实际类型的 Java API。

```groovy
// Groovy 中的单引号文字用于String，而双引号导致 Stringor GString，具体取决于文字中是否存在插值。
static void main(String[] args) {
    def g = "Groovy"

    print "GString $g"
}
```

### 额外的关键字

```groovy
as 
def 
in 
trait 
it // 闭包中默认
```
