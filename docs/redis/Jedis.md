## Jedis

### Maven依赖

```xml
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>3.2.0</version>
</dependency>
```

### 注意事项
1. 禁用Linux的防火墙

```bash
systemctl stop/disable firewalld.service 
```

2. redis.conf中注释掉 bind 127.0.0.1 ,设置 protected-mode no
  













