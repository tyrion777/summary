## 持久化AOF(Append Only File)

### 概念

以日志的形式来记录每个写操作（增量保存），redis启动之初会读取该文件重新构建数据，
换言之，redis 重启的话就根据日志文件的内容将写指令`从前到后`执行一次以完成数据的恢复工作

### 备份过程

`AOF默认不开启`

1. 默认文件名称 appendonly.aof

2. 如果AOF和RDB同时开启，系统默认取AOF的数据

3. 遇到AOF文件损坏，通过/usr/local/bin/redis-check-aof--fix appendonly.aof进行恢复

`AOF持久化流程`

<img :src="$withBase('/AOF持久化流程.png')" alt="AOF持久化流程">

### 同步频率

1. appendfsync always  

始终同步，每次Redis的写入都会立刻记入日志；性能较差但数据完整性比较好

2. appendfsync everysec  

每秒同步，每秒记入日志一次，如果宕机，本秒的数据可能丢失。

3. appendfsync no  
redis不主动进行同步，把同步时机交给操作系统

### 关于详细配置参考 配置文件










